﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class Script_PlayerHealth : NetworkBehaviour
{
    public const int playerMaxHealth = 10;

    [SyncVar]
    public int currentHealth = playerMaxHealth;

    public float timerMax = 2.0f;
    public float timer = 0.0f;
    GameObject player;
    void Awake()
    {
        currentHealth = playerMaxHealth;
    }

    void Update()
    {
        //timer to see when 2 seconds has passed to display players health
        timer += Time.deltaTime;

        if (timer > timerMax)
        { 
            DisplayNameHealth();
            timer = 0;
        }
        
    }

    //take damage with UI buttone clicked
    public void TakeDamage()
    {
        currentHealth -= 1;
        Debug.Log("Take Damage Health:" + currentHealth);
    }

    //take damage with UI buttone clicked

    public void TakeHealth()
    {
        currentHealth += 1;
        Debug.Log("Heal Damage Health: " + currentHealth);
    }
    //output to debug players name and health
    public void DisplayNameHealth()
    {
        Debug.Log("Name " + currentHealth);
    }
}
